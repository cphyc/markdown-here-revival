{
  "app_name": {
    "message": "Markdown Here",
    "description": "This is the name of the extension, the placeholder $APPNAME$ is used the remaining strings."
  },
  "app_slogan": {
    "message": "Write your email in Markdown, then make it pretty."
  },
  "toggle_button_text": {
    "message": "마크다운 토글",
    "description": "Text shown underneath the render toggle button. Make it as short as possible."
  },
  "toggle_button_tooltip": {
    "message": "Click to toggle Markdown rendering",
    "description": "text shown when user hovers mouse over Markdown Toggle button"
  },
  "toggle_button_tooltip_disabled": {
    "message": "The selected field is not valid for Markdown rendering",
    "description": "text shown when user hover mouse over disabled Markdown Toggle button"
  },
  "context_menu_item": {
    "message": "마크다운 토글",
    "description": "Text shown on the context menu."
  },
  "invalid_field": {
    "message": "The selected field is not valid for Markdown rendering. Please use a rich editor.",
    "description": "Shown in an alert dialog if the user tries to render a field that cannot be rendered"
  },
  "plain_text_compose": {
    "message": "You are using a plain-text compose editor. You must change to a rich editor to use Markdown Here.",
    "description": "Shown in an alert dialog if the user has their email compose app set to plain text",
    "placeholders": {
      "appname": { "content": "Markdown Here Revival" }
    }
  },
  "cursor_into_compose": {
    "message": "Please put the cursor into the compose box."
  },
  "forgot_to_render_prompt_title": {
    "message": "Forget to toggle Markdown?",
    "description": "Title of the prompt that's shown when it's detected that a user may have forgotten to render the Markdown in their email"
  },
  "forgot_to_render_prompt_info": {
    "message": "It looks like you wrote this email in Markdown but forgot to make it pretty.",
    "description": "Informational part of the prompt that's shown when it's detected that a user may have forgotten to render the Markdown in their email"
  },
  "forgot_to_render_prompt_question": {
    "message": "이대로 보내시겠습니까?",
    "description": "Question part of the prompt that's shown when it's detected that a user may have forgotten to render the Markdown in their email"
  },
  "forgot_to_render_back_button": {
    "message": "뒤로",
    "description": "Text on the Back (cancel) button of the prompt that's shown when it's detected that a user may have forgotten to render the Markdown in their email"
  },
  "forgot_to_render_send_button": {
    "message": "보내기",
    "description": "Text on the Send (continue) button of the prompt that's shown when it's detected that a user may have forgotten to render the Markdown in their email"
  },
  "forgot_to_render_prompt_close_hover": {
    "message": "Dismiss this notification",
    "description": "Text that shows when the user hovers over the 'X' that will close the prompt"
  },
  "upgrade_notification_text": {
    "message": "Markdown Here 업데이트 되었습니다",
    "description": "Text on the notification shown when Markdown Here has been updated",
    "placeholders": {
      "appname": { "content": "Markdown Here Revival" }
    }
  },
  "upgrade_notification_changes_tooltip": {
    "message": "Click to see the changes in this release",
    "description": "Tooltip text on the link that opens the changes list"
  },
  "upgrade_notification_dismiss_tooltip": {
    "message": "Dismiss this notification",
    "description": "Tooltip text on the link that dismisses the notification"
  },
  "nothing_to_render": {
    "message": "Nothing found to render or revert",
    "description": "Error message shown to user if there's nothing found to revert or render."
  },
  "unrendering_modified_markdown_warning": {
    "message": "The rendered Markdown appears to have been modifed.\nIf you unrender it, your changes since rendering will be lost.\n\nAre you sure you wish to unrender?",
    "description": "Warning shown to the user if she tries to revert some rendered Markdown that they've modified since rendering. The user will lose their modifications if they proceed."
  },
  "options_page__page_title": {
    "message": "Markdown Here Options",
    "placeholders": {
      "appname": { "content": "Markdown Here Revival" }
    }
  },
  "options_page__basic_usage": {
    "message": "기본 사용법"
  },
  "options_page__reload_restart": {
    "message": "Reload your email web page or restart your web browser.",
    "description": "A step in the basic usage instructions"
  },
  "options_page__start_new_message": {
    "message": "Start a new email message.",
    "description": "A step in the basic usage instructions"
  },
  "options_page__type_some_markdown": {
    "message": "Type some Markdown into the email.<br/>Try this: <code>_Hello_ `Markdown` **Here**!</code>"
  },
  "options_page__pretty_cool": {
    "message": "The message should now look pretty cool. Send it to your friends!",
    "description": "A step in the basic usage instructions"
  },
  "options_page__resources_title": {
    "message": "Resources and Links",
    "description": "A section title"
  },
  "options_page__resources_cheatsheet_link": {
    "message": "If you're new to Markdown, the <a href=\"https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet\" target=\"_blank\">Markdown Here Cheatsheet</a> will make you a prostar."
  },
  "options_page__advanced_usage": {
    "message": "For lots more info and advanced usage, see the <a href=\"https://github.com/adam-p/markdown-here\" target=\"_blank\">Markdown Here project page</a>. You should also explore the options below."
  },
  "options_page__ggroup": {
    "message": "Ask a question, start a discussion, or just say hi in the <a href=\"https://groups.google.com/forum/?fromgroups=#!forum/markdown-here\" target=\"_blank\">markdown-here Google Group</a>."
  },
  "options_page__mdh_wiki": {
    "message": "Visit the <a href=\"https://github.com/adam-p/markdown-here/wiki\" target=\"_blank\">Markdown Here wiki</a> for info about <a href=\"https://github.com/adam-p/markdown-here/wiki/Compatibility\" target=\"_blank\">where else MDH works</a> and more <a href=\"https://github.com/adam-p/markdown-here/wiki/Tips-and-Tricks\" target=\"_blank\">tips and tricks</a>."
  },
  "options_page__next_steps": {
    "message": "If you're interested in what the future holds for Markdown Here, check out the <a href=\"https://github.com/adam-p/markdown-here#next-steps\" target=\"_blank\">Next Steps section</a> of the README."
  },
  "options_page__primary_css_title": {
    "message": "Primary Styling CSS",
    "description": "section title"
  },
  "options_page__reset_primary_css": {
    "message": "초기화",
    "description": "text on button to reset Primary Styling CSS"
  },
  "options_page__syntax_highlighting_css_title": {
    "message": "Syntax Highlighting CSS",
    "description": "section title"
  },
  "options_page__syntax_highlighting_theme_label": {
    "message": "테마:",
    "description": "label for the syntax highlighting theme combo box"
  },
  "options_page__preview_title": {
    "message": "미리 보기",
    "description": "section title for the Markdown preview section"
  },
  "options_page__markdown_toggle_button_text": {
    "message": "Markdown Toggle",
    "description": "button text for Markdown Toggle button"
  },
  "options_page__notes_title": {
    "message": "Notes",
    "description": "section title"
  },
  "options_page__note_options_sync": {
    "message": "Changes are automatically saved and synchronized (if sync is enabled in your browser)."
  },
  "options_page__note_syntax_styles_after_primary": {
    "message": "The syntax highlighting styles are applied after the primary styling, and so take precedence."
  },
  "options_page__note_default_client_styles": {
    "message": "Some email editors and browsers impose their own styles. Those may not be apparent here."
  },
  "options_page__tex_math_title": {
    "message": "TeX Mathematical Formulae Support"
  },
  "options_page__tex_math_privacy_issue_1":{
    "message":"To generate the image for the formula, a request is sent to Google. That effectively means that you are sharing your formula with Google. It also means that recipients of your message will be making a request to Google to have the image rendered.",
    "description":"one of the TeX math privacy concerns"
  },
  "options_page__tex_math_privacy_issue_2":{
    "message":"If you customize the formula rendering tag, you should use a secure (https:\/\/) path. Not all rendering services provide a secure option.",
    "description":"one of the TeX math privacy concerns"
  },
  "options_page__hotkey_title": {
    "message": "Hotkey (aka keyboard shortcut)",
    "description": "A section title"
  },
  "options_page__forgot_to_render_2": {
    "message": "When you send an email, this checks the content to see if it looks like you wrote it in Markdown, but forgot to render it (i.e., click \"Markdown Toggle\") before sending. This may need to be disabled if it interferes with your ability to send email.",
    "description": "Descriptive text explaining the 'forgot-to-render' feature."
  },
  "options_page__changes_saved": {
    "message": "변경 내용이 저장되었습니다",
    "description": "message that shows when user changes have been saved"
  },
  "options_page__preview_markdown": {
    "message": "```javascript<br>\nfunction syntaxHighlighting() {<br>\n&nbsp; var n = 33;<br>\n&nbsp; var s = \"hello, こんにちは\";<br>\n&nbsp; console.log(s);<br>\n}<br>\n```<br>\n<br>\n* plain<br>\n* *emphasis*<br>\n&nbsp; * **strong emphasis**<br>\n&nbsp; &nbsp; * ~~strikethrough~~<br>\n* `inline code`<br>\n<br>\n1. Numbered list<br>\n2. [Link](https://www.google.com)<br>\n<br>\n<br>\nAn image: ![Markdown Here logo](/images/icon24.png)\n<br>\n<br>\n> Block quote.&nbsp; <br>\n> *With* **some** `markdown`.<br>\n<br>\nIf **TeX Math** support is enabled, this is the quadratic equation:&nbsp;<br>\n$$-b \\pm \\sqrt{b^2 - 4ac} \\over 2a$$<br>\n<br>\n# Header 1<br>\n## Header 2<br>\n### Header 3<br>\n#### Header 4<br>\n##### Header 5<br>\n###### Header 6<br>\n&nbsp; <br>\n| Tables        | Are           | Cool  |<br>\n| ------------- |:-------------:| -----:|<br>\n| column 3 is   | right-aligned | $$1600 |<br>\n| column 2 is   | centered      |   $$12 |<br>\n| zebra stripes | are neat      |    $$1 |<br>\n<br>\nHere's a horizontal rule:<br>\n<br>\n---<br>\n<br>\n```<br>\ncode block<br>\nwith no highlighting<br>\n```<br>\n<br>\n",
    "description": "This doesn't need to be translated, unless you want to make it more relevant to your language. The raw Markdown used for previewing user styling changes. Also shows users what can be done with Markdown. Please note that the double dollar signs ($$) are needed to make a single dollar sign in the application."
  },
  "currently_in_use": {
    "message": "Currently in use",
    "description": "Indicates the syntax highlighting theme that the user is currently using"
  },
  "options_page__gfm_line_breaks_enabled_label": {
    "message": "<b>Enable GFM line breaks.</b>",
    "description": "Label for the checkbox that enables the support of 'Github-flavored Markdown' line breaks."
  },
  "__WET_LOCALE__": { "message": "ko" }
}
