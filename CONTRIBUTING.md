# Contributing

* Project page: https://gitlab.com/jfx2006/markdown-here-revival

## Code contributions

Markdown Here Revival (for Thunderbird) will use GitLab and whatever tools
that make sense. At the moment that is just the Git repository.

I plan to use the Wiki for documentation.

Bug reports and feature requests can go in the issue tracker, and pull requests
are encouraged!

## Contributor Conduct

I would think that in 2021 wr can be adults and respect one another regardless
of whatever.

### Contributor license agreement

All contributions must be somehow MIT compatible for now; I intend to look at
relicensing so that we don't need to maintain who has the copyright on what where.
It's open source, that sort of thing is silly.

## Translation

I know very little about this stuff other than I have a bunch of messages.json
files that look like they can be cumbersome to manage.

Right now, they're not updated with the few string changes I made. I'm happy
to use some sort of third-party thing like the old
[Crowdin project page](https://crowdin.net/project/markdown-here). Preferably
something that can used for free under a small/OSS project agreement.

## Contributors

* [JFX](https://gitlab.com/jfx2006) - Fork owner and Thunderbird core developer

See [the old CONTRIBUTING file](_prefork/CONTRIBUTING.md) for historical contributors.

### Translators

See [the old CONTRIBUTING file](_prefork/CONTRIBUTING.md) for historical translators.
